This is a very basic implementation of unix domain server.

This is an implementation done with 'poll'.

The goal is to extend/refactor the code according to the following:

this code focuses on the connection aspects and not on the functionality.

Use the client together with netcat: nc -Ul ./unixsock
use the server together with netcat: nc -U ./unixsock

1) use 'select(2)' i.s.o. 'poll(2)'

2) extend the server to accept multiple connections
   AND
   implement a 'syslog' like logging capability. Do the actual implementation
   and do not rely on 'syslog itself'.
   see https://embedded-linux-labs.readthedocs.io/en/latest/lab-posix-01.html#

3) spawn a child process per incoming connection.

4) spawn a thread per incoming connection.
