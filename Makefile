CFLAGS = -Wall -Werror -ggdb -O0

all: server client

%:%.c

clean:
	rm -rf *.o server client
