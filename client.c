#include <err.h>
#include <poll.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/un.h>
#include <sys/socket.h>

#define NUM_FDS 1

static
int open_unix_socket(const char * path)
{
    int fd = 0;
    int r = 0;
    struct sockaddr_un addr = {};

    if ( strlen(path) > (sizeof(addr.sun_path)-1) ) {
        err(1, "open_unix_socket(%s): path too long", path);
    }

    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd < 0) {
        err(1, "socket()");
    }

    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, path, sizeof(addr.sun_path)-1);

    r = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
    if (r < 0) {
        close(fd);
        err(1, "open_unix_socket() bind");
    }

    return fd;
}

static
int on_data (int fd) 
{
    char buff[0x100] = {};
    int r = read(fd, buff, sizeof(buff));
    if (r < 0) {
        warn("read()");
        return 1;
    }
    
    printf("%s", buff);
    
    return 0;
}

static
void clear_revents(struct pollfd * fds)
{
    struct pollfd * e = fds + NUM_FDS;
    for(;fds < e; ++fds) {
        fds->revents = 0;
    }
}

int main(int argc, char *argv[])
{
    int r = 0;

    struct pollfd fds[NUM_FDS] = {};
    fds[0].events = fds[1].events = POLLIN;
    fds[0].fd = open_unix_socket(argc > 1 ? argv[1] : "./unixsock");

    while(1) {

        clear_revents(fds);

        r = poll(fds, NUM_FDS, -1);
        if (r < 0) {
            if (errno == EINTR) {
                continue;
            }
        }

        if (r) {
            if (fds[0].revents & POLLIN) {
                on_data(fds[0].fd);
            }
        }
    }

    return 0;
}
